#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <iostream>


/* Detour */
LPVOID DetourFunc(BYTE* src, const BYTE* dst, const int len);

/* Log / Debug */
VOID InitConsole();
int SetDebugPrivilege(HANDLE hProc);


/* Utility*/
LPCSTR ToHex(DWORD b);



/* Analysis*/
BOOL bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask);
DWORD dwFindPattern(DWORD dwAddress, DWORD dwLen, BYTE *bMask, char * szMask);