#include "thk.h"

/* Log / Debug */
VOID InitConsole()
{
	FILE* stream;
	AllocConsole();
	freopen_s(&stream, "CONOUT$", "w", stdout);
	freopen_s(&stream, "CONIN$", "r", stdin);
}

int SetDebugPrivilege(HANDLE hProc)
{
	TOKEN_PRIVILEGES tp;
	HANDLE hToken;
	LUID luid;

	if (!OpenProcessToken(hProc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		return 0;

	if (!LookupPrivilegeValue("", "SeDebugPrivilege", &luid))
		return 0;

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	return AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), NULL, NULL);
}



/* Utility*/
LPCSTR ToHex(DWORD b)
{
	char* buffer = new char[9];
	_itoa_s(b, buffer, 9, 16);

	return buffer;
}


/* Detour*/
LPVOID DetourFunc(BYTE* src, const BYTE* dst, const int len)
{
	BYTE *jmp = (BYTE*)malloc(len + 5);
	DWORD dwback;
	VirtualProtect(src, len, PAGE_READWRITE, &dwback);
	memcpy(jmp, src, len);
	jmp += len;
	jmp[0] = 0xE9;
	*(DWORD*)(jmp + 1) = (DWORD)(src + len - jmp) - 5;
	src[0] = 0xE9;
	*(DWORD*)(src + 1) = (DWORD)(dst - src) - 5;
	VirtualProtect(src, len, dwback, &dwback);
	return (jmp - len);
}


/* Analysis*/
BOOL bDataCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
{
	for (; *szMask; ++szMask, ++pData, ++bMask)
	{
		if (*szMask == 'x' && *pData != *bMask)
		{
			return false;
		}
	}

	return (*szMask) == NULL;
}

DWORD dwFindPattern(DWORD dwAddress, DWORD dwLen, BYTE *bMask, char * szMask)
{
	for (DWORD i = 0; i < dwLen; i++)
	{
		if (bDataCompare((BYTE*)(dwAddress + i), bMask, szMask))
		{
			std::cout << dwAddress + i << std::endl;
			return (DWORD)(dwAddress + i);
		}
	}

	return 0;
}