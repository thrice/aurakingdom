#pragma once
#include <Windows.h>
#include "Vector3.h"

//SEntityInfo::Type
//lpType 0 = Player
//lpType 1 = Main player + eidolons
//lpType 2 = Treasures / Farm Objects
//lpType 3 = Npc's
//lpType 4 = Unknown
//lpType 5 = Monster

enum EntityType { ET_PLAYER, ET_EIDOLON, ET_OBJECT, ET_NPC, ET_UNK1, ET_MONSTER };

volatile struct EntityInfo
{
	DWORD unk1;//0
	DWORD unk2;//4
	DWORD currentHP;//8
	DWORD cash;//C
	DWORD level;//10
	float moveSpeedReal;//14
	DWORD moveSpeedDisplayed;//18
	DWORD critrate;//1C
	DWORD speed;//20
	DWORD HPmax;//24
	DWORD def;//28
	DWORD eva;//2C
	DWORD critval;//30
	DWORD acc;//34
	DWORD regen;//38
	int unk_0x3C;
	int unk_0x40;
	EntityType type;//44
	DWORD dmg;//48

	//DWORD score;//54

	BYTE unk_[0x1c];

	int currentEXP; //0x68

	BYTE unk_2[0x94];

	char charName[38]; // 0x100
	char guildName[38]; // 0x138 - not sure about size here

	BYTE unk3[0x138];

	int maxHPBonusPercentageViaEquipAndStuff; //0x2A8

	BYTE unk4[0xF8];

	int mailCount; //0x3A4
};

struct Entity
{
	DWORD unk1;
	DWORD unk2;
	DWORD entityID;//8
	EntityInfo *info;//C
	BYTE unk3[0x0000024];//10
	float positionX;// 34 
	float positionY;// 38 
	BYTE unk4[0x0000054];//3C
	DWORD unk;// this is not the target id...
	BYTE unk5[0x00000C4];//94
	Vector3f position;//154
};

struct EntityElement
{
	EntityElement* next;
	DWORD unk1;
	int id;
	Entity* entity;
};

struct EntityContainer
{
	EntityElement* begin;
};

struct EntityCollection
{
	DWORD unk1;
	DWORD unk2;

	EntityContainer *container;// 8
	int nbElements;

	BYTE unk3[0x20];// 10

	char name[0x10];// 30 : name of the collection
	BYTE unk4[0x8];// 40
};

enum EntityCollectionType { ECT_Chara, ECT_Effect, ECT_Duel };


// struct WorldEntity
// {
// 	DWORD unk1;
// 	WorldEntity* prev;//4
// 	WorldEntity* next;//8
// 	DWORD unk2;// C
// 	Entity* entity;//10
// };
// 
// struct WorldList //0x0179D87C
// {
// 	BYTE unk1[ 0x000000A4 ];
// 	WorldEntity *World;//A4
// };


struct SCamera
{
	BYTE unk[0x30];

	float rotationUnk; //0x30 Cam Rotation ?
	float rotationUnk2; //0x34 Cam Rotation ?

	float offsetX; //0x38 Cam Offset X
	float offsetY; //0x3C Cam Offset Y
	float offsetZ; //0x40 Cam Offset Z

	BYTE unk2[0xC];

	float zoomLimitNear; //0x50 Nearest Zoom
	float zoomLimitFar; //0x54 Farthest Zoom // Modify this value for zoom hack

	BYTE unk3[0x28];

	float zoom; //0x84 Current Zoom
};


// Size = 0x140 bytes
// First slot is 0x280 bytes, but somehow only contains the second item (first slot is ommited)
struct InventorySlot
{
	int amount; //0x0
	int hasItem; //0x4

	BYTE unk1[0x18];

	// Connection to .dds file (icon?)
	int itemType; //0x20 -- Gaia Crystal = 0x00340030
	int itemID; //0x24 -- Large Experience Crystal = 0x00390036

	BYTE unk2[0x64];

	int anotherItemID; //0x8C

	BYTE unk3[0x98];

	int isTooltipOpen; //0x128

	BYTE unk4[0x14];
};