/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
#include "CMain.h"

PVOID g_ExceptionHandler = NULL;

std::pair< SMemoryQueryInfo*, ULONG > g_InterceptHolder[ ] =
{
	std::pair< SMemoryQueryInfo*, ULONG >( &m_QueryGame[ 0 ], NULL ),
	std::pair< SMemoryQueryInfo*, ULONG >( &m_QueryGame[ 1 ], NULL ),
	std::pair< SMemoryQueryInfo*, ULONG >( &m_QueryGame[ 2 ], NULL )
};

static INT g_InterceptHolderSize = sizeof( g_InterceptHolder ) / sizeof( g_InterceptHolder[ 0 ] );

LONG WINAPI myExceptionFilter( EXCEPTION_POINTERS* lpExceptionInfo )
{
	if( lpExceptionInfo->ExceptionRecord->ExceptionCode == STATUS_BREAKPOINT )
	{
		for( INT i = 0; i < g_InterceptHolderSize; i++ )
		{
			if( lpExceptionInfo->ExceptionRecord->ExceptionAddress == ( PVOID )g_InterceptHolder[ i ].first->Match[ 0 ] )
			{
				lpExceptionInfo->ContextRecord->Eip = g_InterceptHolder[ i ].second;

				myCodeHandler( lpExceptionInfo->ContextRecord, i );

				break;
			}
		}

		return EXCEPTION_CONTINUE_EXECUTION;
	}

	return EXCEPTION_CONTINUE_SEARCH;
}

VOID ExceptionGenerate( PVOID lpAddress )
{
	ULONG dwOldProtect = NULL;

	if( VirtualProtect( lpAddress, 1, PAGE_EXECUTE_READWRITE, &dwOldProtect ) )
	{
		*( PBYTE )lpAddress = 0xCC;

		VirtualProtect( lpAddress, 1, dwOldProtect, &dwOldProtect );
	}
}

HRESULT UpdateInterceptHolder( )
{
	for( INT i = 0x00000000; i < g_InterceptHolderSize; i++ )
	{
		if( g_InterceptHolder[ i ].first->Match.empty( ) )
		{
			return E_UNEXPECTED;
		}

		if( g_InterceptHolder[ i ].second == NULL )
		{
			g_InterceptHolder[ i ].second = g_InterceptHolder[ i ].first->Match[ 0 ] + g_InterceptHolder[ i ].first->Misc;
		}
	}

	return NO_ERROR;
}

HRESULT DetourClientCode( )
{
	if( FAILED( UpdateInterceptHolder( ) ) ){
		return E_UNEXPECTED;
	}

	g_ExceptionHandler = AddVectoredExceptionHandler( 0xDEADBABE, myExceptionFilter );

	if( g_ExceptionHandler == NULL ){
		return E_UNEXPECTED;
	}

	for( INT i = 0x00000000; i < g_InterceptHolderSize; i++ ){
		ExceptionGenerate( ( PVOID )g_InterceptHolder[ i ].first->Match[ 0 ] );
	}

	return NO_ERROR;
}


