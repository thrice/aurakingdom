/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
typedef struct  
{
	LPSTR Name;
	PBYTE Signature;
	ULONG Size;
	ULONG Delta;
	ULONG Misc;
	BYTE WildCard;
	BOOL IsPtr;
	BOOL IsRelative;
	std::vector< ULONG > Match;
} SMemoryQueryInfo;

typedef struct  
{
	LPSTR Name;
	SMemoryQueryInfo *Info;
	ULONG Size;
} SMemoryModuleQuery;

extern SMemoryQueryInfo m_QueryGame[ ];

BOOL myMemoryReadSignature( ULONG lpAddress, PBYTE lpSig, INT lpSize, BYTE lpWildCard );
VOID myMemoryUpdateInfo( );
SMemoryQueryInfo* myGetMemoryQueryInfo( LPCSTR lpName );
HRESULT myDebugQueryMemory( );

