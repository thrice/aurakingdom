/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
#include "CMain.h"

ULONG myGetPlayerTarget( ULONG lpBase )
{
	ULONG dwRes = NULL;

	if( lpBase )
	{
		__asm
		{
			mov eax, lpBase;
			mov eax, [ eax + 0x0000004C ];
			mov dwRes, eax;
		}
	}

	return dwRes;
}

ULONG mySelectNearestTarget( ULONG lpBase, ULONG lpFunction )
{
	ULONG dwRes = NULL;

	if( lpBase )
	{
		__asm
		{
			mov esi, lpBase;
			push 0x00000001;
			mov ecx, esi;
			call lpFunction;
			mov dwRes, eax;
		}
	}

	return dwRes;
}

ULONG myMoveToPosition( FLOAT lpX, FLOAT lpY, ULONG lpFunction )
{
	ULONG dwRes = NULL;

	__asm
	{
		mov eax, lpFunction;
		call eax;
		cmp eax, dwRes;
		je Finnish;
		mov ecx, eax;
		mov eax, [ eax ];
		mov eax, [ eax + 0x00000014 ];
		push lpY;
		push lpX;
		call eax;
		mov dwRes, eax;
Finnish:
		;
	}

	return dwRes;
}

ULONG myGetSkillIdFromSlotBar( ULONG lpSlot, ULONG lpBase )
{
	ULONG dwDelta = ( lpSlot - 1 ) * 4, dwRes = NULL;

	if( lpBase )
	{
		__asm
		{
			mov edi, dwDelta;
			mov eax, lpBase;
			mov eax, [ eax + edi ];
			mov dwRes, eax;
		}
	}

	return dwRes;
}

ULONG mySendSkillID( ULONG lpSkillId, ULONG lpBase, ULONG lpFunction )
{
	ULONG dwRes = NULL;

	if( lpBase )
	{
		__asm
		{
			mov edi, lpBase;
			mov eax, 0x00000000;
			mov esi, lpSkillId;
			push eax;
			push eax;
			mov eax, [ esi + 0x08 ];
			shr eax, 0x0C;
			and eax, 0x0000FFFF;
			push eax;
			mov ecx, edi;
			call lpFunction;
			mov dwRes, eax;
		}
	}

	return dwRes;
}

ULONG mySalvageItem( ULONG lpSlotId, ULONG lpFunction )
{
	ULONG dwRes = NULL;

	__asm
	{
		mov ecx, lpFunction;
		push 0x00000000;
		push lpSlotId;
		push 0x00000000;
		call ecx;
		mov dwRes, eax;
	}

	return dwRes;
}

VOID myTeleportToPos( ULONG lpBase, FLOAT lpX, FLOAT lpY, FLOAT lpZ )
{
	ULONG dwPtr = *( PULONG )lpBase;

	if( dwPtr == NULL )
		return;

	dwPtr = *( PULONG )( dwPtr + 0x14 );

	if( dwPtr == NULL )
		return;

	dwPtr = *( PULONG )( dwPtr + 0x64 );

	if( dwPtr == NULL )
		return;

	dwPtr = *( PULONG )( dwPtr + 0x10 );

	if( dwPtr == NULL )
		return;

	dwPtr = *( PULONG )( dwPtr + 0x10 );

	if( dwPtr == NULL )
		return;

	*( PFLOAT )( dwPtr + 0x154 ) = lpX;
	*( PFLOAT )( dwPtr + 0x158 ) = lpY;
	*( PFLOAT )( dwPtr + 0x15C ) = lpZ;
}