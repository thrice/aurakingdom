/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/

//#define MACRO_DEBUGEVENT

#pragma comment( lib, "psapi.lib" )
#pragma comment( lib, "..\\Shared\\libs\\lua52.lib" )
#pragma comment( lib, "..\\Shared\\libs\\detours.lib" )

#include <windows.h>
#include <math.h>
#include <psapi.h>
#include <intrin.h>

#include <vector>
#include <string>

EXTERN_C
{
	#include "..\\Shared\\libs\\lua.h"
	#include "..\\Shared\\libs\\lualib.h"
	#include "..\\Shared\\libs\\lauxlib.h"
};

#include "..\\Shared\\libs\\detours.h"

#include "CMemoryHandler.h"
#include "CClientDevice.h"
#include "CClientDetour.h"
#include "CCodeHandler.h"
#include "CLuaDevice.h"
