/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
VOID myCodeHandler( PCONTEXT lpContext, INT lpMessage );

INT CGetPlayerTarget( lua_State *lpArgument );
INT CSelectNearestTarget( lua_State *lpArgument );
INT CMoveToPosition( lua_State *lpArgument );
INT CGetSlotSkillId( lua_State *lpArgument );
INT CSendSkillId( lua_State *lpArgument );
INT CSalvageItem( lua_State *lpArgument );

