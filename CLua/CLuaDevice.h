/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
class CLuaDevice
{
private:
	lua_State *dwLuaState;
public:
	CLuaDevice( );
	virtual ~CLuaDevice( );
	HRESULT RegisterObjects( );
	HRESULT RegisterObjects( const luaL_Reg *lpArgument );
	HRESULT RegisterFunctions( const luaL_Reg *lpArgument );
	HRESULT ExecuteMacro( LPCSTR lpName );
};