/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
#include "CMain.h"

BOOL g_Macro = FALSE;

SMemoryQueryInfo* g_ClientFunctions[ ] =
{
	&m_QueryGame[ 3 ],
	&m_QueryGame[ 4 ],
	&m_QueryGame[ 5 ],
	&m_QueryGame[ 6 ]
};

SMemoryQueryInfo* g_GameData[ ] =
{
	&m_QueryGame[ 8 ],
	&m_QueryGame[ 7 ]
};

std::vector< ULONG > g_QueryTunelArgs;

ULONG g_QueryTunel[ 2 ] = { 
	NULL, 
	NULL 
};

INT CGetPlayerTarget( lua_State *lpArgument )
{
	g_QueryTunel[ 0 ] = 1;

	while( 1 )
	{
		if( g_QueryTunel[ 0 ] == 0 )
		{
			lua_pushinteger( lpArgument, ( INT )g_QueryTunel[ 1 ] );
			g_QueryTunel[ 1 ] = 0;
			break;
		}

		Sleep( 10 );
	}

	return 1;
}

INT CSelectNearestTarget( lua_State *lpArgument )
{
	g_QueryTunel[ 0 ] = 2;

	while( 1 )
	{
		if( g_QueryTunel[ 0 ] == 0 )
		{
			lua_pushinteger( lpArgument, ( INT )g_QueryTunel[ 1 ] );
			g_QueryTunel[ 1 ] = 0;
			break;
		}

		Sleep( 10 );
	}

	return 1;
}

INT CMoveToPosition( lua_State *lpArgument )
{
	g_QueryTunelArgs.push_back( ( ULONG )lua_tonumber( lpArgument, 1 ) );
	g_QueryTunelArgs.push_back( ( ULONG )lua_tonumber( lpArgument, 2 ) );

	g_QueryTunel[ 0 ] = 3;

	while( 1 )
	{
		if( g_QueryTunel[ 0 ] == 0 )
		{
			lua_pushinteger( lpArgument, ( INT )g_QueryTunel[ 1 ] );
			g_QueryTunel[ 1 ] = 0;
			break;
		}

		Sleep( 10 );
	}

	return 1;
}

INT CGetSlotSkillId( lua_State *lpArgument )
{
	g_QueryTunelArgs.push_back( ( ULONG )lua_tointeger( lpArgument, 1 ) );

	g_QueryTunel[ 0 ] = 4;

	while( 1 )
	{
		if( g_QueryTunel[ 0 ] == 0 )
		{
			lua_pushinteger( lpArgument, ( INT )g_QueryTunel[ 1 ] );
			g_QueryTunel[ 1 ] = 0;
			break;
		}

		Sleep( 10 );
	}

	return 1;
}

INT CSendSkillId( lua_State *lpArgument )
{
	g_QueryTunelArgs.push_back( ( ULONG )lua_tointeger( lpArgument, 1 ) );

	g_QueryTunel[ 0 ] = 5;

	while( 1 )
	{
		if( g_QueryTunel[ 0 ] == 0 )
		{
			lua_pushinteger( lpArgument, ( INT )g_QueryTunel[ 1 ] );
			g_QueryTunel[ 1 ] = 0;
			break;
		}

		Sleep( 10 );
	}

	return 1;
}

INT CSalvageItem( lua_State *lpArgument )
{
	g_QueryTunelArgs.push_back( ( ULONG )lua_tointeger( lpArgument, 1 ) );

	g_QueryTunel[ 0 ] = 6;

	while( 1 )
	{
		if( g_QueryTunel[ 0 ] == 0 )
		{
			lua_pushinteger( lpArgument, ( INT )g_QueryTunel[ 1 ] );
			g_QueryTunel[ 1 ] = 0;
			break;
		}

		Sleep( 10 );
	}

	return 1;
}

VOID myCodeHandler( PCONTEXT lpContext, INT lpMessage )
{
	switch( lpMessage ) 
	{
		case 0: 
		{
			lpContext->Edx = *( PULONG )( lpContext->Ecx + 0x000000A8 );//EndScene

		} break;

		case 1: 
		{
			lpContext->Edx = *( PULONG )lpContext->Edi;

			if( g_QueryTunel[ 0 ] &&
				g_QueryTunel[ 1 ] == NULL &&
				g_Macro )
			{
				if( lpContext->Edx == g_GameData[ 1 ]->Match[ 0 ] )
				{
					switch( g_QueryTunel[ 0 ] ) 
					{
						case 0x00000001: 
						{
							g_QueryTunel[ 1 ] = myGetPlayerTarget( *( PULONG )g_GameData[ 0 ]->Match[ 0 ] );
							g_QueryTunelArgs.clear( );
							g_QueryTunel[ 0 ] = 0;
						} break;

						case 0x00000002: 
						{
							g_QueryTunel[ 1 ] = mySelectNearestTarget( *( PULONG )g_GameData[ 0 ]->Match[ 0 ], g_ClientFunctions[ 0 ]->Match[ 0 ] );
							g_QueryTunelArgs.clear( );
							g_QueryTunel[ 0 ] = 0;
						} break;

						case 0x00000003: 
						{
							g_QueryTunel[ 1 ] = myMoveToPosition( ( FLOAT )g_QueryTunelArgs[ 0 ], ( FLOAT )g_QueryTunelArgs[ 1 ], g_ClientFunctions[ 2 ]->Match[ 0 ] );
							g_QueryTunelArgs.clear( );
							g_QueryTunel[ 0 ] = 0;
						} break;

						case 0x00000005: 
						{
							g_QueryTunel[ 1 ] = mySendSkillID( g_QueryTunelArgs[ 0 ], lpContext->Edi, g_ClientFunctions[ 1 ]->Match[ 0 ] );
							g_QueryTunelArgs.clear( );
							g_QueryTunel[ 0 ] = 0;
						} break;

						case 0x00000006: 
							{
								g_QueryTunel[ 1 ] = mySalvageItem( g_QueryTunelArgs[ 0 ], g_ClientFunctions[ 3 ]->Match[ 0 ] );
								g_QueryTunelArgs.clear( );
								g_QueryTunel[ 0 ] = 0;
							} break;

						default: {
						} break;
					}
				}
			}
		} break;

		case 2: 
		{
			lpContext->Edx = *( PULONG )( lpContext->Esi + 0x00000004 );

			if( g_QueryTunel[ 0 ] &&
				g_QueryTunel[ 1 ] == NULL &&
				g_Macro )
			{
				if( g_QueryTunel[ 0 ] == 0x00000004 )
				{
					g_QueryTunel[ 1 ] = myGetSkillIdFromSlotBar( g_QueryTunelArgs[ 0 ], lpContext->Edx );
					g_QueryTunelArgs.clear( );
					g_QueryTunel[ 0 ] = 0;
				}
			}
		} break;

		default: {
		} break;
	}
}