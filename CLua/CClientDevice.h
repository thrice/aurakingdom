/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
ULONG myGetPlayerTarget( ULONG lpBase );
ULONG mySelectNearestTarget( ULONG lpBase, ULONG lpFunction );
ULONG myMoveToPosition( FLOAT lpX, FLOAT lpY, ULONG lpFunction );
ULONG myGetSkillIdFromSlotBar( ULONG lpSlot, ULONG lpBase );
ULONG mySendSkillID( ULONG lpSkillId, ULONG lpBase, ULONG lpFunction );
ULONG mySalvageItem( ULONG lpSlotId, ULONG lpFunction );
VOID myTeleportToPos( ULONG lpBase, FLOAT lpX, FLOAT lpY, FLOAT lpZ );

typedef struct
{
	BYTE Private00[ 0x0000008 ];
	INT CurHP;//8
	BYTE Private01[ 0x0000018 ];
	INT MaxHP;//24
	BYTE Private02[ 0x000001C ];
	INT Type;//44
	BYTE Private03[ 0x00000B8 ];
	char Name[ 0x20 ];//100
} SEntityInfo;

typedef struct 
{
	ULONG Base;
	BYTE Private00[ 0x0000004 ];
	ULONG Id;//8
	SEntityInfo *Info;//C
	BYTE Private01[ 0x0000080 ];
	ULONG TargetId;//90
	BYTE Private02[ 0x00000C0 ];
	FLOAT PosX;//154
	FLOAT PosY;//158
	FLOAT PosZ;//15C
} SEntity;