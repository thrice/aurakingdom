/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
#include "CMain.h"

CLuaDevice::CLuaDevice( )
{
}

CLuaDevice::~CLuaDevice( )
{
	if( dwLuaState == NULL )
		return;

	lua_close( dwLuaState );
}

HRESULT CLuaDevice::RegisterObjects( )
{
	dwLuaState = luaL_newstate( );

	if( dwLuaState == NULL )
		return E_UNEXPECTED;

	luaL_openlibs( dwLuaState );

	return NO_ERROR;
}

HRESULT CLuaDevice::RegisterObjects( const luaL_Reg *lpArgument )
{
	dwLuaState = luaL_newstate( );

	if( dwLuaState == NULL )
		return E_UNEXPECTED;

	for( ; lpArgument->func != NULL; lpArgument++ )
	{
		luaL_requiref( dwLuaState, lpArgument->name, lpArgument->func, 1 );
		lua_settop( dwLuaState, 0 );
	}
	return NO_ERROR;
}

HRESULT CLuaDevice::RegisterFunctions( const luaL_Reg *lpArgument )
{
	if( dwLuaState == NULL )
		return E_UNEXPECTED;

	for( ; lpArgument->func != NULL; lpArgument++ )
	{
		lua_register( dwLuaState, lpArgument->name, lpArgument->func );
	}
	return NO_ERROR;
}

HRESULT CLuaDevice::ExecuteMacro( LPCSTR lpName )
{
	if( dwLuaState == NULL )
		return E_UNEXPECTED;

	return luaL_dofile( dwLuaState, lpName ) ? E_UNEXPECTED : NO_ERROR;
}