/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
#include "CMain.h"

LPCSTR g_AppTitle = "CLuaDevice";

std::pair< HANDLE, ULONG > g_WorkerThread;

CLuaDevice* g_LuaDevice = NULL;

extern BOOL g_Macro;

FLOAT g_PlayerPosition[ 3 ] = {
	NULL,
	NULL,
	NULL
};

VOID myUpdatePosition( ULONG lpBase )
{
	ULONG dwPtr = *( PULONG )lpBase;

	if( dwPtr == NULL )
		return;

	dwPtr = *( PULONG )( dwPtr + 0x14 );

	if( dwPtr == NULL )
		return;

	dwPtr = *( PULONG )( dwPtr + 0x64 );

	if( dwPtr == NULL )
		return;

	dwPtr = *( PULONG )( dwPtr + 0x10 );

	if( dwPtr == NULL )
		return;

	dwPtr = *( PULONG )( dwPtr + 0x10 );

	if( dwPtr == NULL )
		return;

	g_PlayerPosition[ 0 ] = *( PFLOAT )( dwPtr + 0x154 );
	g_PlayerPosition[ 1 ] = *( PFLOAT )( dwPtr + 0x158 );
	g_PlayerPosition[ 2 ] = *( PFLOAT )( dwPtr + 0x15C );
}

INT CSleep( lua_State *lpArgument )
{
	Sleep( lua_tointeger( lpArgument, 1 ) );

	lua_pushinteger( lpArgument, 1 );

	return 1;
}

INT CTeleportToLocalPos( lua_State *lpArgument )
{
	myTeleportToPos( m_QueryGame[ 10 ].Match[ 0 ], g_PlayerPosition[ 0 ], g_PlayerPosition[ 1 ], g_PlayerPosition[ 2 ] );

	lua_pushinteger( lpArgument, 1 );

	return 1;
}

DWORD WINAPI myWorkerThread( LPVOID lpArg )
{
	UNREFERENCED_PARAMETER( lpArg );

	g_LuaDevice = new CLuaDevice( );

	try
	{
		if( g_LuaDevice )
		{
			static luaL_Reg dwFunctions [ ] =
			{
				{ "CGetPlayerTarget", CGetPlayerTarget },
				{ "CMoveToPosition", CMoveToPosition },
				{ "CSelectNearestTarget", CSelectNearestTarget },
				{ "CGetSlotSkillId", CGetSlotSkillId },
				{ "CSendSkillId", CSendSkillId },
				{ "CSalvageItem", CSalvageItem },
				{ "CTeleportToLocalPos", CTeleportToLocalPos },	
				{ "CSleep", CSleep }
			};

			if( SUCCEEDED( g_LuaDevice->RegisterObjects( ) ) &&
				SUCCEEDED( g_LuaDevice->RegisterFunctions( dwFunctions ) ) )
			{
				ULONG dwOldProt = NULL;

				if( VirtualProtect( ( PVOID )m_QueryGame[ 9 ].Match[ 0 ], 4, PAGE_EXECUTE_READWRITE, &dwOldProt ) )
				{
					*( PFLOAT )m_QueryGame[ 9 ].Match[ 0 ] = 3600.0f;
					VirtualProtect( ( PVOID )m_QueryGame[ 9 ].Match[ 0 ], 4, dwOldProt, &dwOldProt );
				}

				while( g_WorkerThread.first )
				{
					if( GetAsyncKeyState( VK_F10 ) &1 )
					{
						ULONG dwProcId = NULL;

						HWND dwWindow = GetForegroundWindow( );

						if( GetWindowThreadProcessId( dwWindow, &dwProcId ) &&
							dwProcId == GetCurrentProcessId( ) )
						{
							g_Macro =! g_Macro;
							
							MessageBoxExA( dwWindow, g_Macro ? "On" : "Off", "ntKid[Aura Kingdom]", NULL, NULL );
							
							if( g_Macro )
							{
								myUpdatePosition( m_QueryGame[ 10 ].Match[ 0 ] ); 
							}
						}
					}

					if( g_Macro )
					{
						g_LuaDevice->ExecuteMacro( "Macro.lua" );
					}

					Sleep( 10 );
				}
			}else{
				throw 1;
			}
		}else{
			throw 0;
		}
	}
	catch( INT lpError )
	{
		char dwMsg[ MAX_PATH ];
		sprintf_s( dwMsg, "WORKERTHREAD[ %i ]", lpError );
		MessageBoxA( NULL, dwMsg, g_AppTitle, NULL );
	}

	if( g_LuaDevice ){
		delete g_LuaDevice;
		g_LuaDevice = NULL;
	}

	return EXIT_SUCCESS;
}

__declspec( dllexport ) BOOL WINAPI DllMain( HANDLE lpModule, DWORD lpReason, LPVOID lpReserved )
{
	UNREFERENCED_PARAMETER( lpReserved );

	if( lpReason == DLL_PROCESS_ATTACH )
	{
		try
		{
			myMemoryUpdateInfo( );

			if( FAILED( myDebugQueryMemory( ) ) ){
				throw 0;
			}
			if( FAILED( DetourClientCode( ) ) ){
				throw 1;
			}

			g_WorkerThread.first = CreateThread( NULL, NULL, ( LPTHREAD_START_ROUTINE )myWorkerThread, ( PVOID )lpModule, NULL, &g_WorkerThread.second );

			if( g_WorkerThread.first == NULL ){
				throw 2;
			}
		}
		catch( INT lpError )
		{
			char dwMsg[ MAX_PATH ];
			sprintf_s( dwMsg, "INJECTTHREAD[ %i ]", lpError );
			MessageBoxExA( NULL, dwMsg, g_AppTitle, NULL, NULL );
		}
	}

	return TRUE;
}