/*
	CLuaDevice for AuraKingdom
	Credits:
	-ntKid
*/
#include "CMain.h"

SMemoryQueryInfo m_QueryGame[ ] =
{
	//BREAKPOINTS
	{
		"Direct3DDevice",
		( PBYTE )"\x8B\x91\xA8\x00\x00\x00\x50\xFF\xD2\x85\xC0\x7D\xEE\x32\xC0\xC3\xB0\x01\xC3",
		19,
		0,
		6,
		0xEE,
		FALSE,
		FALSE,
		std::vector< ULONG >( 0 )
	},
	{
		"ExecuteSpellId",
		( PBYTE )"\x8B\x17\x8B\x92\xC8\x00\x00\x00\x51\x50\x8B\xCF\xFF\xD2\x84\xC0\x0F\x85\xEE",
		19,
		0,
		2,
		0xEE,
		FALSE,
		FALSE,
		std::vector< ULONG >( 0 )
	},
	{
		"UpdateBarSpell",		  
		( PBYTE )"\x8B\x56\x04\x8B\x04\xBA\x85\xC0\x74\x50\x8B\x50\x08\xC1\xEA\x1C\x83\xFA\x08",
		19,
		0,
		3,
		0xEE,
		FALSE,
		FALSE,
		std::vector< ULONG >( 0 )
	},//2
	//FUNCTIONS
	{
		"SelectEntityId",		  
		( PBYTE )"\x55\x8B\xEC\x8B\x45\x08\x56\x8B\xF1\x01\x46\x58\x83\x3D\xEE\xEE\xEE\xEE\xEE",
		19,
		0,
		0,
		0xEE,
		FALSE,
		FALSE,
		std::vector< ULONG >( 0 )
	},
	{
		"CastBarSpellId",
		( PBYTE )"\xE8\xEE\xEE\xEE\xEE\x6A\x00\x8B\xCE\xE8\xEE\xEE\xEE\xEE\x5F\xC7\x86\x0C\x03",
		19,
		0,
		0,
		0xEE,
		FALSE,
		TRUE,
		std::vector< ULONG >( 0 )
	},
	{
		"MoveToPosition",
		( PBYTE )"\xE8\xEE\xEE\xEE\xEE\x8B\x7D\xF8\x8B\x4D\xF4\x3B\x79\x04\x8B\xF0\x75\xEE\xE8",
		19,
		0,
		0,
		0xEE,
		FALSE,
		TRUE,
		std::vector< ULONG >( 0 )
	},
	{
		"SalvageSlotPos",	
		( PBYTE )"\x0F\xB7\x86\x10\x02\x00\x00\x0F\xB6\x8E\x12\x02\x00\x00\x50\x51\x6A\x00\xE8",
		19,
		0x12,
		0,
		0xEE,
		FALSE,
		TRUE,
		std::vector< ULONG >( 0 )
	},//6
	//DATA
	{
		"IsReadySpellId",	
		( PBYTE )"\xEE\xEE\xEE\xEE\x8D\x86\x7C\x03\x00\x00\x50\xC7\x45\xFC\x12\x00\x00\x00\xE8",
		19,
		0,
		0,
		0xEE,
		TRUE,
		FALSE,
		std::vector< ULONG >( 0 )
	},
	{
		"PlayerInfoBase",	
		( PBYTE )"\xEE\xEE\xEE\xEE\x52\xE8\xEE\xEE\xEE\xEE\x8B\x5B\x2C\x8B\x43\x40\xC1\xE8\x09",
		19,
		0,
		0,
		0xEE,
		TRUE,
		FALSE,
		std::vector< ULONG >( 0 )
	},
	{
		"TargetFovPatch",
		( PBYTE )"\xEE\xEE\xEE\xEE\xD9\x45\xE4\xD8\xD1\xDF\xE0\xDD\xD9\xF6\xC4\x41\x74\xEE\xD8",
		19,
		0,
		0,
		0xEE,
		TRUE,
		FALSE,
		std::vector< ULONG >( 0 )
	},
	{
		"AvatarPosition",
		( PBYTE )"\xEE\xEE\xEE\xEE\x3B\xC3\x75\xEE\xE8\xEE\xEE\xEE\xEE\x8B\x8E\x70\x01\x00\x00",
		19,
		0,
		0,
		0xEE,
		TRUE,
		FALSE,
		std::vector< ULONG >( 0 )
	}//10
};

SMemoryModuleQuery m_ModuleQuery[ ] =
{
	{
		"game.bin",
		m_QueryGame,
		sizeof( m_QueryGame ) / 
		sizeof( m_QueryGame[ 0 ] )
	}
};

static INT m_ModuleQuerySize = sizeof( m_ModuleQuery ) / sizeof( m_ModuleQuery[ 0 ] );

BOOL myMemoryReadSignature( ULONG lpAddress, PBYTE lpSig, INT lpSize, BYTE lpWildCard )
{
	for( INT i = 0; i < lpSize; i++ )
	{
		BYTE dwCur = *( PBYTE )( lpAddress + i );

		if( dwCur == lpSig[ i ] || 
			lpSig[ i ] == lpWildCard )
		{

		}else{
			return FALSE;
		}
	}	
	return TRUE;
}

VOID myMemoryUpdateCurAddress( ULONG lpAddress, ULONG lpSize, SMemoryQueryInfo* lpSignature )
{
	if( ( lpAddress + lpSignature->Size ) < lpSize )
	{
		if( myMemoryReadSignature( lpAddress, lpSignature->Signature, lpSignature->Size, lpSignature->WildCard ) )
		{
			ULONG dwPos = lpAddress + lpSignature->Delta;

			if( lpSignature->IsRelative )
			{
				__asm
				{
					mov eax, dwPos;
					mov esi, [ eax + 1 ];
					add eax, esi;
					add eax, 0x00000005;
					mov dwPos, eax;
				}
			}

			if( lpSignature->IsPtr )
			{
				dwPos = *( PULONG )dwPos;
			}

			lpSignature->Match.push_back( dwPos );
		}
	}
}

VOID myMemoryUpdateInfo( )
{
	for( ULONG i = 0x00000000; i < m_ModuleQuerySize; i++ )
	{
		MODULEINFO dwInfo;
		ZeroMemory( &dwInfo, sizeof( MODULEINFO ) );

		if( GetModuleInformation( GetCurrentProcess( ), LoadLibraryA( m_ModuleQuery[ i ].Name ), 
			&dwInfo, sizeof( MODULEINFO ) ) )
		{
			ULONG dwSize = ( ULONG )dwInfo.lpBaseOfDll + dwInfo.SizeOfImage;

			for( ULONG j = ( ULONG )dwInfo.lpBaseOfDll; j < dwSize; j++ )
			{
				for( ULONG c = 0x00000000; c < m_ModuleQuery[ i ].Size; c++ )
				{
					myMemoryUpdateCurAddress( j, dwSize, &m_ModuleQuery[ i ].Info[ c ] );
				}
			}
		}
	}
}

SMemoryQueryInfo* myGetMemoryQueryInfo( LPCSTR lpName )
{
	for( ULONG i = 0x00000000; i < m_ModuleQuerySize; i++ )
	{
		for( ULONG c = 0x00000000; c < m_ModuleQuery[ i ].Size; c++ )
		{
			SMemoryQueryInfo* dwQuery = &m_ModuleQuery[ i ].Info[ c ];

			if( strcmp( m_ModuleQuery[ i ].Info[ c ].Name, lpName ) == NULL )
			{
				return dwQuery;
			}
		}
	}
	return NULL;
}

HRESULT myDebugQueryMemory( )
{
	HRESULT dwRes = NO_ERROR;

	for( ULONG i = 0x00000000; i < m_ModuleQuerySize; i++ )
	{
		for( ULONG c = 0x00000000; c < m_ModuleQuery[ i ].Size; c++ )
		{
			SMemoryQueryInfo* dwQuery = &m_ModuleQuery[ i ].Info[ c ];

			if( dwQuery->Match.size( ) )
			{
#ifdef MACRO_DEBUGEVENT
				for( INT x = 0; x < dwQuery->Match.size( ); x++ )
				{
					FILE* dwLog = fopen( "DEBUG.LOG", "a+" );

					if( dwLog )
					{
						fprintf( dwLog, "%s %s 0x%X\n", m_ModuleQuery[ i ].Name, dwQuery->Name, dwQuery->Match[ x ] );
						fclose( dwLog );
					}
				}
#endif	
			}
			else
			{
#ifdef MACRO_DEBUGEVENT
				FILE* dwLog = fopen( "DEBUG.LOG", "a+" );

				if( dwLog )
				{
					fprintf( dwLog, "%s %s 0x%X\n", m_ModuleQuery[ i ].Name, dwQuery->Name, 0 );
					fclose( dwLog );
				}
#endif	
				dwRes = E_UNEXPECTED;
			}
		}
	}
	return dwRes;
}