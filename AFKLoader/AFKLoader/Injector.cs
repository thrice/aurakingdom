﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace AFKLoader
{
    public static class Injector
    {
        public static Boolean InjectDll(IntPtr hProc, String szDllPath, IntPtr lpLLAddress)
        {
            if (!File.Exists(szDllPath))
            {
                Console.WriteLine("Dll Path error");
                return false;
            }

            IntPtr lpAddress = Winapi.VirtualAllocEx(hProc, (IntPtr)null, (IntPtr)szDllPath.Length, (0x2000 | 0x1000), 0x04);

            if (lpAddress == Winapi.INTPTR_ZERO)
            {               
                Console.WriteLine("VirtualAllocEx Failed:" + Marshal.GetLastWin32Error());
                return false;
            }

            byte[] bytes = Encoding.ASCII.GetBytes(szDllPath);

            if (Winapi.WriteProcessMemory(hProc, lpAddress, bytes, (uint)bytes.Length, 0) == 0)
            {
                Console.WriteLine("WriteProcessMem failed");
                Console.WriteLine(Marshal.GetLastWin32Error());
                return false;
            }

            if (Winapi.CreateRemoteThread(hProc, (IntPtr)null, Winapi.INTPTR_ZERO, lpLLAddress, lpAddress, 0, (IntPtr)null) == Winapi.INTPTR_ZERO)
            {                
                Console.WriteLine("CreateRemoteThread failed");
                Console.WriteLine(Marshal.GetLastWin32Error());
                return false;
            }

            return true;
        }
    }
}
