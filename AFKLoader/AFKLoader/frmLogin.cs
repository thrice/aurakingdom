﻿using System;
using System.Windows.Forms;

namespace AFKLoader
{
    public partial class frmLogin : Form
    {
        public string Response { get; set; }

        public frmLogin()
        {
            InitializeComponent();
            this.Response = String.Empty;
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {            
            webBrowser1.Navigate("https://www.aeriagames.com/dialog/oauth?response_type=code&client_id=18457ae92576b65ab92213612f6cc02d051ef19d2&state=xyz&&lang=us&Scope=scope_general");
            webBrowser1.Navigated += FetchResponse;
        }


        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (this.Response == String.Empty)
            switch (MessageBox.Show("Exit AFKLoader?", "Heads up", MessageBoxButtons.YesNo))
            {
                case DialogResult.Yes:
                    this.Response = "EXIT";
                    break;
                default:
                    e.Cancel = true;
                    break;
            }
        }

        private void FetchResponse(object sender, EventArgs e)
        {            
            var query = webBrowser1.Url.Query.Split('=');
            if (query.Length <= 3)
            {
                this.Response = query[2];
                this.Close();
            }
        }
    }
}
