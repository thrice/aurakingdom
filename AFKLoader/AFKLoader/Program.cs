﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace AFKLoader
{
    static class Program
    {

        public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        public static bool SetDebugPrivilege(IntPtr hProc)
        {
	          TOKEN_PRIVILEGES tp;
	          IntPtr hToken;
	          LUID luid;

              if (!Winapi.OpenProcessToken(hProc, (0x0020 | 0x0008), out hToken))
                return false;
    
             if(!Winapi.LookupPrivilegeValue(null, "SeDebugPrivilege", out luid))
                return false;
   
             tp.PrivilegeCount         = 1;
             tp.Luid     = luid;
             tp.Attributes = 0x00000002;

             return Winapi.AdjustTokenPrivileges(hToken, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
        }

        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Console.WriteLine(Environment.OSVersion.Version.Major);
            if (IsAdministrator())
            {

                SetDebugPrivilege(Winapi.GetCurrentProcess());
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                frmLogin login = new frmLogin();

                XmlDocument doc = new XmlDocument();
                doc.Load(@"afkloaderconf.xml");

                login.ShowDialog();
                if (login.Response != "EXIT" && login.Response != String.Empty)
                {
                    string launchParams = " EasyFun -a " + login.Response + " -p xlwlogin -l -f -g -s";

                    STARTUPINFO si = new STARTUPINFO();
                    PROCESS_INFORMATION pi = new PROCESS_INFORMATION();

                    Winapi.CreateProcess("game.bin", launchParams, IntPtr.Zero, IntPtr.Zero, false, ProcessCreationFlags.CREATE_SUSPENDED, IntPtr.Zero, null, ref si, out pi);


                    IntPtr hMainThread = pi.hThread;
                    Console.WriteLine("Process Created");

                    if (hMainThread == Winapi.INTPTR_ZERO)
                    {
                        Console.WriteLine("Handle error");
                        Console.WriteLine(Marshal.GetLastWin32Error());
                    }


                    var dlls = new List<String>();
                    foreach (XmlNode dll in doc.GetElementsByTagName("dll"))
                        dlls.Add(dll.InnerText);

                    uint dwDesiredAccess = (0x0002 | 0x0008 | 0x0010 | 0x0020);
                    if (Environment.OSVersion.Version.Major > 5)
                    {
                        dwDesiredAccess = 0xFFFF;
                    }


                    var lpLoadLibraryAddress = Winapi.GetProcAddress(Winapi.GetModuleHandle("kernel32.dll"), "LoadLibraryA");
                    IntPtr hProc = Winapi.OpenProcess(dwDesiredAccess, 0, pi.dwProcessId);

                    if (hProc == Winapi.INTPTR_ZERO)
                    {
                        Console.WriteLine("hProc error:" + Marshal.GetLastWin32Error());
                    }

                    if (lpLoadLibraryAddress == Winapi.INTPTR_ZERO)
                        Console.WriteLine("LoadLib failed GetProcAddress");

                    var error = false;

                    foreach (var dll in dlls)
                    {
                        if (!Injector.InjectDll(hProc, dll, lpLoadLibraryAddress))
                            error = true;
                    }

                    if (error)
                        MessageBox.Show("1 or more Dlls failed to inject =/");

                    Winapi.ResumeThread(hMainThread);
                    Console.WriteLine("Resuming Thread");

                    Winapi.CloseHandle(hProc);
                    Winapi.CloseHandle(hMainThread);
                }
                Application.Exit();
            }
            else
            {
                MessageBox.Show("Must be run as admin!");
                Application.Exit();
            }
        }
    }
}
