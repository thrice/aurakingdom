#include <thk.h>
#include "offsetdata.h"

VOID Run();
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	// Perform actions based on the reason for calling.
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
	{
		InitConsole();
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)&Run, NULL, 0, 0);
		break;
	}
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}

DWORD FindOffset(SOffsetData* offsetData);

VOID Run()
{
	std::cout << "=== PRESS ENTER TO START DUMPING ===" << std::endl;
	std::cin.get();

	SOffsetDatas offsetDatas = GetOffsetDatas();

	FILE *log = fopen("AKOffsetDump.log", "w+");

	for (UINT i = 0; i < offsetDatas.size(); i++)
	{
		std::cout << offsetDatas[i]->name << ": 0x" << ToHex(FindOffset(offsetDatas[i])) << std::endl;
		fprintf_s(log, "%s: 0x%s\n", offsetDatas[i]->name, ToHex(FindOffset(offsetDatas[i])));
	}

	fclose(log);
	std::cout << "\n=== DONE SCANNING ===" << std::endl;
	std::cin.get();

	FreeConsole();
} 

DWORD FindOffset(SOffsetData* offsetData)
{
	DWORD dwRes;

	DWORD dwBaseAddress = dwFindPattern(0x00400000, 0x009C888A, (PBYTE)offsetData->pattern, offsetData->mask);
	DWORD dwRealAddress = dwBaseAddress + offsetData->offset;


	if (offsetData->useReinterpretCast)
	{
		dwRes = *reinterpret_cast<DWORD*>(dwRealAddress)+(offsetData->isRelative ? (dwRealAddress + 4) : 0);
	}
	else dwRes = (dwRealAddress)+(offsetData->isRelative ? (dwRealAddress + 4) : 0);
	

	return dwRes;
}