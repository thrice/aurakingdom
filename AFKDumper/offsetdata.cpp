#include "offsetdata.h"

SOffsetDatas GetOffsetDatas()
{
	std::vector<SOffsetData*> offsetDatas;

	// GetPlayerStructFunction
	offsetDatas.push_back(new SOffsetData(
		"GetPlayerStructFunction",
		"\x0F\x85\x9A\x00\x00\x00\x8B\x46\x40",
		"xxxxxxxxx",
		0x1E,
		true,
		true
	));

	// CameraStruct
	offsetDatas.push_back(new SOffsetData(
		"CameraStruct",
		"\x51\x8B\x48\x04\x52\x89\x5D\xF8\xE8\xCE\x21\xE3\xFF",
		"xxxxxxxxxxxxx",
		-0x4,
		false,
		true
	));

	// MoveToPosition
	offsetDatas.push_back(new SOffsetData(
		"MoveToPosition",
		"\xE8\x87\xF1\xB8\xFF\x83\xC4\x04\x89\x45\xF0\x85\xC0",
		"xxxxxxxxxxxxx",
		-0x24,
		false,
		false
	));

	return offsetDatas;
}
