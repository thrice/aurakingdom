#include <Windows.h>
#include <vector>

struct SOffsetData
{
	LPSTR name;
	char* pattern;
	char* mask;
	DWORD offset;
	bool isRelative;
	bool useReinterpretCast;

	SOffsetData(LPSTR name, char* pattern, char* mask, DWORD offset = 0, bool isRelative = false, bool useReinterpretCast = false)
	{
		this->name = name;
		this->pattern = pattern;
		this->mask = mask;
		this->offset = offset;
		this->isRelative = isRelative;
		this->useReinterpretCast = useReinterpretCast;
	}
};

typedef std::vector<SOffsetData*> SOffsetDatas;

SOffsetDatas GetOffsetDatas();
