#include "thk.h"
#include <objects.h>

struct Addresses 
{
	// Pointers
	static const DWORD CameraStruct = 0x00F3B21C;

	/*	 
	IDA Byte Pattern: 0F 85 9A 00 00 00 8B 46 40
		First called function:

		jnz     loc_762E7D
		call    sub_6D8460 <<<< this is the function
		cmp     dword ptr [eax+260h], 0
		jg      short loc_762E3F 
	*/
	static const DWORD GetPlayerStructFunction = 0x006D8460; // IDA BytePattern: 0F 85 9A 00 00 00 8B 46 40
};


VOID Run();
SCamera* InitZoomH();
