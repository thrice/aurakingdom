#include "thk.h"
#include "afk.h"
#include "antimutex.h"

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	// Perform actions based on the reason for calling.
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
	{
		//InitConsole();
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)&Run, NULL, 0, 0);
		break;
	}
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}

SCamera* InitZoomH()
{
	SCamera* cam = *(SCamera**)Addresses::CameraStruct;
	cam->zoomLimitFar = 50;

	return cam;
}

EntityInfo* GetCharacterStatsStruct(DWORD lpFunction = Addresses::GetPlayerStructFunction)
{
	DWORD dwRes;

	__asm
	{
		call lpFunction;
		mov dwRes, eax;
	}

	return (EntityInfo*)dwRes;
}

VOID Run()
{
	KillMutex();

	Sleep(10000);

	InitZoomH();

	EntityInfo* character = NULL;
	bool bRunForestRun = false;
	DWORD dwProcId;

	DWORD customMoveSpeedDisplayed = 175;

	while (true)
	{
		if (GetWindowThreadProcessId(GetForegroundWindow(), &dwProcId) && dwProcId == GetCurrentProcessId())
		{
			Sleep(50);

			if (GetAsyncKeyState(VK_F11) & 1) { bRunForestRun = true; customMoveSpeedDisplayed = 175; }
			if (GetAsyncKeyState(VK_F12) & 1) { bRunForestRun = false; }
			if (GetAsyncKeyState(VK_OEM_PLUS) & 1) { customMoveSpeedDisplayed += 5; }
			if (GetAsyncKeyState(VK_OEM_MINUS) & 1) { customMoveSpeedDisplayed -= 5; }


			if (bRunForestRun)
			{
				character = GetCharacterStatsStruct();

				if (character)
				{
					character->moveSpeedReal = (customMoveSpeedDisplayed * 0.085f);
					character->moveSpeedDisplayed = customMoveSpeedDisplayed;
				}
			}
		}
	}
}