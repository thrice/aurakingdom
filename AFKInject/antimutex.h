#include <Windows.h>
#include "libs\detours.h"

#pragma once
#pragma comment (lib,__FILE__ "/../libs/detours.lib")

typedef HANDLE(WINAPI* t_OpenMutex)(DWORD, BOOL, LPCSTR);
typedef HANDLE(WINAPI* t_CreateMutex)(LPSECURITY_ATTRIBUTES, BOOL, LPCSTR);

VOID KillMutex();