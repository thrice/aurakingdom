#include "antimutex.h"

t_OpenMutex o_OpenMutex = NULL;
t_CreateMutex o_CreateMutex = NULL;


HANDLE WINAPI hk_CreateMutex(LPSECURITY_ATTRIBUTES lpMutexAttributes, BOOL bInitialOwner, LPCSTR lpName)
{
	return o_CreateMutex(lpMutexAttributes, bInitialOwner, "abc");
}

HANDLE WINAPI hk_OpenMutex(DWORD dwDesiredAccess, BOOL bInheritHandle, LPCSTR lpName)
{
	return o_OpenMutex(dwDesiredAccess, bInheritHandle, "aaaaa");
}

// Enable Multieclient
VOID KillMutex()
{
	HINSTANCE hModule = NULL;

	hModule = GetModuleHandleA("kernel32.dll");

	FARPROC dwOpenMutex = GetProcAddress(hModule, "OpenMutexA");
	o_OpenMutex = (t_OpenMutex)DetourFunction((PBYTE)dwOpenMutex, (PBYTE)hk_OpenMutex);

	FARPROC dwCreateMutex = GetProcAddress(hModule, "CreateMutexA");
	o_CreateMutex = (t_CreateMutex)DetourFunction((PBYTE)dwCreateMutex, (PBYTE)hk_CreateMutex);
}